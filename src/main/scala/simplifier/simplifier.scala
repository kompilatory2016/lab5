package simplifier


import AST._

import scala.math.pow

// to implement
// avoid one huge match of cases
// take into account non-greedy strategies to resolve cases with power laws
object Simplifier {

  def simplify(node: Node): Node = node match {

    //recognize tuples
    case BinExpr("+", Tuple(a), Tuple(b)) => Tuple(a ++ b)

    case BinExpr(oper, left, right) => (oper, simplify(left), simplify(right)) match {
      //recognize power laws
      case ("*", BinExpr("**", l1, r1), BinExpr("**", l2, r2)) if simplify(l1) == simplify(l2) =>
        BinExpr("**", l1, BinExpr("+", r1, r2))
      case ("**", x, IntNum(a)) if a == 0 => IntNum(1)
      case ("**", x, IntNum(a)) if a == 1 => x
      case ("**", BinExpr("**", x, y), m) => BinExpr("**", x, BinExpr("**", y, m))
      case ("+", BinExpr("**", x1, IntNum(a)), BinExpr("+", BinExpr("*", BinExpr("*", x2, IntNum(b)), y1), BinExpr("**", y2, IntNum(c))))
        if 2 == a && a == b && a == c && simplify(x1) == x2 && simplify(y1) == y2 =>
        BinExpr("**", BinExpr("+", x1, y1), IntNum(2))
      case ("-", BinExpr("-", BinExpr("**", BinExpr("+", x1, y1), IntNum(a)), BinExpr("**", x2, IntNum(b))),
      BinExpr("*", BinExpr("*", IntNum(c), x3), y2))
        if a == 2 && b == a && a == c && simplify(x1) == simplify(x2) && simplify(x1) == simplify(x3) && simplify(y1) == simplify(y2) => BinExpr("**", y1, IntNum(2))
      case ("-", BinExpr("**", BinExpr("+", x1, y1), IntNum(a)), BinExpr("**", BinExpr("-", x2, y2), IntNum(b)))
        if a == 2 && b == a && simplify(x1) == simplify(x2) && simplify(y1) == simplify(y2) => BinExpr("*", BinExpr("*", IntNum(4), x1), y1)

      case ("+", IntNum(a), IntNum(b)) => IntNum(a + b)
      case ("-", IntNum(a), IntNum(b)) => IntNum(a - b)
      case ("*", IntNum(a), IntNum(b)) => IntNum(a * b)
      case ("/", IntNum(a), IntNum(b)) => IntNum(a / b)
      case ("**", IntNum(a), IntNum(b)) => IntNum(pow(a.toDouble, b.toDouble).toInt)
      case ("**", IntNum(a), BinExpr("**", IntNum(b), IntNum(c))) => IntNum(pow(b.toDouble, pow(a.toDouble, c.toDouble)).toInt)
      case ("<", IntNum(a), IntNum(b)) => if (a < b) TrueConst() else FalseConst()
      case ("<=", IntNum(a), IntNum(b)) => if (a <= b) TrueConst() else FalseConst()
      case (">", IntNum(a), IntNum(b)) => if (a > b) TrueConst() else FalseConst()
      case (">=", IntNum(a), IntNum(b)) => if (a >= b) TrueConst() else FalseConst()
      case ("==", IntNum(a), IntNum(b)) => if (a == b) TrueConst() else FalseConst()
      case ("!=", IntNum(a), IntNum(b)) => if (a != b) TrueConst() else FalseConst()

      case ("+", FloatNum(a), FloatNum(b)) => FloatNum(a + b)
      case ("-", FloatNum(a), FloatNum(b)) => FloatNum(a - b)
      case ("*", FloatNum(a), FloatNum(b)) => FloatNum(a * b)
      case ("/", FloatNum(a), FloatNum(b)) => FloatNum(a / b)

      //simplify expressions
      case ("<", FloatNum(a), FloatNum(b)) => if (a < b) TrueConst() else FalseConst()
      case ("<=", FloatNum(a), FloatNum(b)) => if (a <= b) TrueConst() else FalseConst()
      case (">", FloatNum(a), FloatNum(b)) => if (a > b) TrueConst() else FalseConst()
      case (">=", FloatNum(a), FloatNum(b)) => if (a >= b) TrueConst() else FalseConst()
      case ("==", FloatNum(a), FloatNum(b)) => if (a == b) TrueConst() else FalseConst()
      case ("!=", FloatNum(a), FloatNum(b)) => if (a != b) TrueConst() else FalseConst()

      case ("+", IntNum(a), b) if a == 0 => b
      case ("+", a, IntNum(b)) if b == 0 => a
      case ("-", a, IntNum(b)) if b == 0 => a
      case ("-", a, b) if a == b => IntNum(0)
      case ("+", Unary("-", a), b) => simplify(BinExpr("-", b, a))
      case ("*", IntNum(a), b) if a == 1 => b
      case ("*", a, IntNum(b)) if b == 1 => a
      case ("*", IntNum(a), b) if a == 0 => IntNum(0)
      case ("*", a, IntNum(b)) if b == 0 => simplify(BinExpr("*", IntNum(b), a))
      case ("or", a, b) if simplify(a) == simplify(b) => a
      case ("and", a, b) if a == b => a
      case ("or", x, TrueConst()) => TrueConst()
      case ("or", x, FalseConst()) => x
      case ("and", x, FalseConst()) => FalseConst()
      case ("and", x, TrueConst()) => x
      case ("==", x, y) if simplify(x) == simplify(y) => TrueConst()
      case (">=", x, y) if simplify(x) == simplify(y) => TrueConst()
      case ("<=", x, y) if simplify(x) == simplify(y) => TrueConst()
      case ("!=", x, y) if simplify(x) == simplify(y) => FalseConst()
      case (">", x, y) if simplify(x) == simplify(y) => FalseConst()
      case ("<", x, y) if simplify(x) == simplify(y) => FalseConst()

      // understand commutativity
      case ("-", BinExpr(op1, a, IntNum(c)), b) if (op1 == "+" || op1 == "-") && simplify(a) == simplify(b) => IntNum(c)
      case ("and", BinExpr("or", a, b), BinExpr("or", c, d))
        if simplify(a) == simplify(d) && simplify(b) == simplify(c) => BinExpr("or", a, b)
      case ("or", BinExpr("and", a, b), BinExpr("and", c, d))
        if simplify(a) == simplify(d) && simplify(b) == simplify(c) => BinExpr("and", a, b)

      // understand distributive property of multiplication
      case (o1, BinExpr("*", IntNum(a), x), y) if o1 == "-" || o1 == "+" =>
        simplify(y) match {
          case x2 if simplify(x) == simplify(x2) => simplify(BinExpr("*", BinExpr(o1, IntNum(a), IntNum(1)), x))
          case BinExpr("*", IntNum(b), x2) if simplify(x) == simplify(x2) => simplify(BinExpr("*", BinExpr(o1, IntNum(a), IntNum(b)), x))
        }
      case (o2, BinExpr("*", x, z), BinExpr("*", y, z2))
        if simplify(z) == simplify(z2) => BinExpr("*", BinExpr(o2, x, y), z)
      case (o3, BinExpr("*", x, y), BinExpr("*", x2, z))
        if simplify(x) == simplify(x2) => BinExpr("*", x, BinExpr(o3, y, z))
      case ("+", BinExpr("+", BinExpr("*", x, BinExpr("+", y, z)), BinExpr("*", vv, yy)), BinExpr("*", v, zz))
        if simplify(vv) == simplify(v) && simplify(zz) == simplify(z) => BinExpr("*", BinExpr("+", x, v), BinExpr("+", y, z))

      //simplify division
      case ("/", x, y) =>
        (simplify(x), simplify(y)) match {
          case (x, y) if x == y => IntNum(1)
          case (BinExpr("+", fl, fr), BinExpr("+", sl, sr)) if fl == sr && fr == sl => IntNum(1)
          case (one, BinExpr("/", x, y)) if one == IntNum(1) && x == IntNum(1) => y
          case (x, y) => BinExpr("/", x, y)
        }
      case ("*", z, BinExpr("/", x, y)) if simplify(x) == IntNum(1) => BinExpr("/", z, y)
      //concatenate lists
      case ("+", ElemList(a), ElemList(b)) => ElemList(a ++ b)
      case ("+", ElemList(List()), b) => simplify(b)
      case ("+", a, ElemList(List())) => simplify(a)

      case (_, a, b) => BinExpr(oper, simplify(a), simplify(b))
    }
    case Unary(op, expr) => (op, simplify(expr)) match {
      // cancel double unary ops
      case ("not", TrueConst()) => FalseConst()
      case ("not", FalseConst()) => TrueConst()
      case ("-", Unary("-", x)) => simplify(x)
      case ("not", Unary("not", x)) => simplify(x)


      // get rid of not before comparisons
      case ("not", BinExpr(op, x, y)) => op match {
        case "==" => simplify(BinExpr("!=", x, y))
        case "!=" => simplify(BinExpr("==", x, y))
        case ">" => simplify(BinExpr("<=", x, y))
        case "<" => simplify(BinExpr(">=", x, y))
        case ">=" => simplify(BinExpr("<", x, y))
        case "<=" => simplify(BinExpr(">", x, y))
      }
      case (op, expr) => Unary(op, expr)
    }

    case KeyDatumList(keyData) => keyData match {
      case head :: tail =>
        if (tail.exists((keyDatum: KeyDatum) => head.key == keyDatum.key)) simplify(KeyDatumList(tail.reverse))
        else simplify(KeyDatumList(tail)) match {
          case KeyDatumList(simplifiedTail) => KeyDatumList((head :: simplifiedTail).reverse)
          case _ => throw new Error("Something went wrong")
        }
      case Nil => KeyDatumList(keyData)
    }
    case Assignment(left, right) => (simplify(left), simplify(right)) match {
      case (left, right) if left == right => NodeList(List())
      case (left, right) => Assignment(left, right)
    }
    case IfElseInstr(cond, left, right) =>
      (simplify(cond), left, right) match {
        case (TrueConst(), _, _) => simplify(left)
        case (FalseConst(), _, _) => simplify(right)
        case (cond, left, right) => IfElseInstr(cond, simplify(left), simplify(right))
      }
    case IfElseExpr(cond, left, right) =>
      (simplify(cond), simplify(left), simplify(right)) match {
        case (TrueConst(), left, right) => left
        case (FalseConst(), left, right) => right
        case (cond, left, right) => IfElseExpr(cond, left, right)
      }
    case WhileInstr(cond, body) => simplify(cond) match {
      case FalseConst() => NodeList(List())
      case _ => WhileInstr(simplify(cond), simplify(body))
    }
    case NodeList(list) =>

      val list_simplified = list map simplify
      // remove dead assignments
      val removed_assgn = list_simplified.foldRight(Nil: List[Node]) { (next, acc) =>
        next match {
          case Assignment(left, right) => acc.filter{x => x.isInstanceOf[Assignment] && x.asInstanceOf[Assignment].left == left}
          case _ => next :: acc
        }
      }
      if (removed_assgn.size == 1) removed_assgn.head else NodeList(removed_assgn)

    case any => any
  }
}
